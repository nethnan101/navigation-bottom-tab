import React from "react";
import {Text, View} from 'react-native'

export default function Settingscreen ({navigation}) {
    return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 26, fontWeight: 'bold'}}>Settings Screen</Text>
        </View>
    )
}
