import React from "react";
import {Text, View} from 'react-native'

export default function Homescreen ({navigation}) {
    return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontSize: 26, fontWeight: 'bold'}}>Home Screen</Text>
        </View>
    )
}
