import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Screens
import Homescreen from '../screens/home';
import Settingscreen from '../screens/settings';
import Detailscreen from '../screens/detail';

// Screens Name
const homename = 'Home';
const detailname = 'Details';
const settingname = 'Settings';

const Tab = createBottomTabNavigator();

export default function MainScreen () {
    return (
       <NavigationContainer>
           <Tab.Navigator
                initialRouteName={homename}
                screenOptions={({route}) => ({
                    tabBarIcon: ({focused, color, size}) => {
                        let iconName;
                        let rn = route.name;
                        if (rn === homename) {
                            iconName = focused ? 'home' : 'home-outline'
                        } else if (rn === detailname) {
                            iconName = focused ? 'list' : 'list-outline'
                        } else if (rn === settingname) {
                            iconName = focused ? 'settings' : 'settings-outline'
                        }
                        return <Ionicons name={iconName} size={size} color={color} />
                    },
                    headerShown: false,
                })}
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'grey',
                    labelStyle: {paddingBottom: 10, fontSize: 10},
                    style: {padding: 10, height: 70},
                }}
            >
                <Tab.Screen name={homename} component={Homescreen}/>
                <Tab.Screen name={detailname} component={Detailscreen}/>
                <Tab.Screen name={settingname} component={Settingscreen}/>
            </Tab.Navigator>
       </NavigationContainer>
    )
}