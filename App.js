import * as React from 'react';
import MainScreen from './navigation/main';

export default function App() {
  return (
    <MainScreen />
  );
}
